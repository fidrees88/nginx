FROM nginx:1.27.3-alpine

COPY *.sh /

# Install cron, certbot, bash, plus any other dependencies
RUN apk add bash wget python3 python3-dev py3-pip build-base libressl-dev musl-dev libffi-dev rust cargo openssl \
    && pip3 install pip --upgrade --break-system-packages \
    && pip3 install certbot-nginx --break-system-packages \
    && chmod +x /*.sh \
    && mkdir /etc/nginx/conf.d/sites-available \
    && mkdir /etc/nginx/ssl \
    && openssl dhparam -out /etc/nginx/ssl/dhparam.pem 4096 \
    && cp /cert-renew.sh /etc/periodic/daily/cert-renew
    
COPY /config/etc/nginx/nginx.conf /etc/nginx/nginx.conf
COPY /config/etc/nginx/conf.d /etc/nginx/conf.d
COPY /config/etc/nginx/templates /etc/nginx/templates
COPY /config/etc/letsencrypt /etc/letsencrypt
COPY docker-entrypoint.sh /

ENV USE_LETSENCRYPT="false"
ENV MAX_BODY_SIZE="1024m"
ENV NX_PROXY_SEND_TIMEOUT="60s"
ENV NX_PROXY_CONNECT_TIMEOUT="60s"
ENV NX_PROXY_READ_TIMEOUT="60s"
ENV NX_PROXY_BUFFER_SIZE="128k"
ENV NX_PROXY_BUFFERS="4 256k"
ENV NX_PROXY_BUSY_BUFFERS_SIZE="256k"
ENV NX_SSL_CERT_CIPHERS="ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-GCM-SHA384 OLD_TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256 OLD_TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256"


ENTRYPOINT ["/docker-entrypoint.sh"]