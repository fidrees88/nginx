#!/bin/sh

/etc/nginx/templates/create_config.sh
/etc/nginx/templates/create-virtual-hosts.sh

set -eu

if [[ $USE_LETSENCRYPT && $USE_LETSENCRYPT == "true" ]];then
  echo "USE_LETSENCRYPT flag is set - will start cert registering in 15 seconds!"
  (sleep 15 && /cert-register.sh) &
  crond && nginx -g 'daemon off;'
else
  nginx -g 'daemon off;'
fi
