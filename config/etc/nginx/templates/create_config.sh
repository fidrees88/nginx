#!/bin/bash

# Allow CORS for Domains
ALL_CORS_DOMAINS=""
for CORS_DOMAIN in $CORS_DOMAINS;
  do
    if [[ -n $CORS_DOMAIN ]];then
      ALL_CORS_DOMAINS="${ALL_CORS_DOMAINS}|${CORS_DOMAIN}"
    fi
  done;

if [[ -n $ALL_CORS_DOMAINS ]];then
  export ALL_CORS_DOMAINS;
  envsubst '${ALL_CORS_DOMAINS}' < /etc/nginx/templates/cors.template > /etc/nginx/conf.d/cors.conf
fi

# Allow iFrame Domains
if [[ -n $FRAME_DOMAINS ]];then
  export FRAME_DOMAINS;
  envsubst '${FRAME_DOMAINS}' < /etc/nginx/templates/common.template > /etc/nginx/conf.d/common.conf
fi

# Set SSL Cipher Suites
if [[ -n $NX_SSL_CERT_CIPHERS ]];then
  export NX_SSL_CERT_CIPHERS;
  envsubst '${NX_SSL_CERT_CIPHERS}' < /etc/nginx/templates/ssl.template > /etc/nginx/conf.d/ssl/ssl.conf
fi

export MAX_BODY_SIZE;
export NX_PROXY_READ_TIMEOUT;
export NX_PROXY_SEND_TIMEOUT;
export NX_PROXY_CONNECT_TIMEOUT
export NX_PROXY_BUFFER_SIZE
export NX_PROXY_BUFFERS
export NX_PROXY_BUSY_BUFFERS_SIZE

envsubst '${MAX_BODY_SIZE} ${NX_PROXY_READ_TIMEOUT} ${NX_PROXY_SEND_TIMEOUT} ${NX_PROXY_CONNECT_TIMEOUT} ${NX_PROXY_BUFFER_SIZE} ${NX_PROXY_BUFFERS} ${NX_PROXY_BUSY_BUFFERS_SIZE}' < /etc/nginx/templates/common-location.template > /etc/nginx/conf.d/common-location.conf
