#!/bin/bash

# Create Wildcard Host for port 80
for WILDCARD_DOMAIN in $WILDCARD_DOMAINS;
  do
    if [ $WILDCARD_DOMAIN ];then
      HOST_DOMAIN=${WILDCARD_DOMAIN//./\\.}
      export HOST_DOMAIN
      envsubst '${HOST_DOMAIN}' < /etc/nginx/templates/wildcard-host.template > /etc/nginx/conf.d/sites-available/wildcard-$WILDCARD_DOMAIN.conf
    fi
  done;

# Create Wildcard Host with SSL
for WILDCARD_DOMAIN in $WILDCARD_DOMAINS_SSL;
  do
    if [ $WILDCARD_DOMAIN ];then
      HOST_DOMAIN=${WILDCARD_DOMAIN//./\\.}
      
      REDIRECT_HTTPS_PORT=""
      if [ $NGINX_HTTPS_PORT != "443" ];then
        REDIRECT_HTTPS_PORT=":$NGINX_HTTPS_PORT"
      fi
      
      export REDIRECT_HTTPS_PORT
      export HOST_DOMAIN
      export WILDCARD_DOMAIN
      envsubst '${REDIRECT_HTTPS_PORT} ${HOST_DOMAIN} ${WILDCARD_DOMAIN}' < /etc/nginx/templates/wildcard-host_ssl.template > /etc/nginx/conf.d/sites-available/wildcard-ssl-$WILDCARD_DOMAIN.conf
    fi
  done;

# Create Virtual Host for Port 80
printf '%s\n' "$PROXY_HOSTS" |
while IFS=, read -r HOSTID SERVER_UPSTREAM SERVER_NAMES
do
  if [ $HOSTID ];then
    echo "$SERVER_UPSTREAM and $SERVER_NAMES"
    export SERVER_UPSTREAM
    export SERVER_NAMES
    envsubst '${SERVER_UPSTREAM} ${SERVER_NAMES}' < /etc/nginx/templates/virtual-host.template > /etc/nginx/conf.d/sites-available/$HOSTID.conf
  fi
done

# Create Virtual Host for Port 443
printf '%s\n' "$PROXY_HOSTS_SSL" |
while IFS=, read -r HOSTID SERVER_UPSTREAM SERVER_NAMES
do
  if [ $HOSTID ];then
    echo "$SERVER_UPSTREAM and $SERVER_NAMES"
    export SERVER_UPSTREAM
    export SERVER_NAMES
    REDIRECT_HTTPS_PORT=""
    if [ $NGINX_HTTPS_PORT != "443" ];then
      REDIRECT_HTTPS_PORT=":$NGINX_HTTPS_PORT"
    fi
    export REDIRECT_HTTPS_PORT
    envsubst '${REDIRECT_HTTPS_PORT} ${SERVER_UPSTREAM} ${SERVER_NAMES}' < /etc/nginx/templates/virtual-host_ssl.template > /etc/nginx/conf.d/sites-available/$HOSTID.conf
  fi
done