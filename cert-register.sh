#!/bin/sh

CERT_DIR="/etc/letsencrypt/live";
printf '%s\n' "$PROXY_HOSTS_SSL" > tmp.file
while IFS=, read -r HOSTID SERVER_UPSTREAM SERVER_NAMES
do
    DOMAINS=${SERVER_NAMES// /,}
    echo $DOMAINS
    # create certificates 
    certbot certonly --nginx --non-interactive --agree-tos --expand --email $LETSENCRYPT_MAIL -d $DOMAINS
    # if certificate was created, update nginx config
    if [ -f "${CERT_DIR}/${HOSTID}/fullchain.pem" ]; then
      sed -i "s/dummy-cert/$HOSTID/" "/etc/nginx/conf.d/sites-available/$HOSTID.conf"
    fi
done < tmp.file
 
nginx -s reload;
