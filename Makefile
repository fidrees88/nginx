SHELL := /bin/bash
GIT_VERSION ?= ${shell git describe --tags --long --always}
GIT_BRANCH ?= ${shell git rev-parse --abbrev-ref HEAD}
GIT_TAG ?= ${shell git describe --tags --always}

GIT_COMMIT_SHORT_SHA := $(if $(GIT_COMMIT_SHORT_SHA),$(GIT_COMMIT_SHORT_SHA),$(GIT_VERSION))
GIT_BRANCH_NAME := $(if $(GIT_BRANCH_NAME),$(GIT_BRANCH_NAME),$(GIT_BRANCH))
GIT_TAG_NAME := $(if $(GIT_TAG_NAME),$(GIT_TAG_NAME),$(GIT_TAG))

LOCAL_IMAGE_NAME=${shell basename `git rev-parse --show-toplevel`}
IMAGE := $(if $(DOCKER_REGISTRY_PATH),$(DOCKER_REGISTRY_PATH),$(LOCAL_IMAGE_NAME))
SERVICE_NAME=nginx

version:
	echo $(IMAGE)
	echo $(GIT_COMMIT_SHORT_SHA)
	echo $(GIT_BRANCH_NAME)
	echo $(GIT_TAG_NAME)

push:
	docker build --no-cache -f Dockerfile . -t $(IMAGE):latest
	docker login -u $(DOCKER_REGISTRY_USER) -p $(DOCKER_REGISTRY_PASSWORD) $(DOCKER_REGISTRY)
	docker push $(IMAGE):latest

release:
	docker build --no-cache -f Dockerfile . -t $(IMAGE):$(GIT_TAG_NAME)
	docker login -u $(DOCKER_REGISTRY_USER) -p $(DOCKER_REGISTRY_PASSWORD) $(DOCKER_REGISTRY)
	docker push $(IMAGE):$(GIT_TAG_NAME)

clean:
	docker system --prune --all

